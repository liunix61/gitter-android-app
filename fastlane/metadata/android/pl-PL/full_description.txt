Gitter to miejsce gdzie programiści przychodzą porozmawiać. Zapewniamy bezpłatny dostęp do publicznych pokojów czatowych dla społeczności programistów i projektów open source, jak i prywatne czaty dla zespołów technicznych i firm.

KLUCZOWE CECHY
- Nieograniczona liczba publicznych pokojów czatowych za darmo
- Nieograniczona historia czatu z mozliwoscia przeszukiwania
- Nieograniczona integracje
- Zbudowany na GitHub, największej sieci na świecie dla programistów
- Prywatne pokoje czatowe za darmo do 25 użytkowników

ZAUFALI NAM
Gitter jest domem dla ponad 30.000 społeczności programistów, w tym Fundacji .NET, Google Materiał Design, angularjs, kręgosłup, node.js, Scala, W3C i wielu innych.

PROBLEMY? FEEDBACK?
Im więcej nam powiesz, tym lepszy stanie się Gitter. W razie jakichkolwiek pytań, i aby dowiedzieć się więcej o produkcie, odwiedź naszą stronę: https://gitter.zendesk.com

Można również zostawic nam informacje w kanale Gitter HQ: https://gitter.im/orgs/gitterHQ
Daj nam znać jak możemy się poprawić!
