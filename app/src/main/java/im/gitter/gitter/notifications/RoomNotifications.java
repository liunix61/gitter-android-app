package im.gitter.gitter.notifications;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.app.RemoteInput;
import android.support.v4.os.BuildCompat;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.VolleyError;

import java.util.List;

import im.gitter.gitter.R;
import im.gitter.gitter.activities.MainActivity;
import im.gitter.gitter.utils.AvatarUtils;

public class RoomNotifications {

    public static final String REMOTE_INPUT_REPLY_KEY = "REPLY";
    public static final String ROOM_ID_INTENT_KEY = "ROOM_ID";
    private static final String TAG = RoomNotifications.class.getSimpleName();

    private final Context context;
    private final Resources res;
    private final String roomId;

    public RoomNotifications(Context context, String roomId) {
        this.context = context;
        this.res = context.getResources();
        this.roomId = roomId;
    }

    public void update(Listener listener) {
        update(false, listener);
    }

    public void updateSilently(Listener listener) {
        update(true, listener);
    }

    private void update(final boolean silent, final Listener listener) {

        Log.i(TAG, "fetching assets for notifications about " + roomId);
        new NotificationAssetsFetcher(context, roomId, new NotificationAssetsFetcher.Listener() {
            @Override
            public void onSuccess(String userDisplayName, String roomUri, boolean isOneToOne, Bitmap roomAvatar, List<NotificationAssetsFetcher.Message> messages, boolean hasMentions) {

                NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                if (messages.size() > 0) {
                    Notification notification = createNotification(silent, userDisplayName, roomUri, isOneToOne, roomAvatar, messages, hasMentions);
                    Log.i(TAG, "showing/updating a notification for "+ roomId);
                    notificationManager.notify(roomId, roomId.hashCode(), notification);
                } else {
                    Log.i(TAG, "no unread messages, clearing notifications for " + roomId);
                    notificationManager.cancel(roomId, roomId.hashCode());
                }

                listener.onFinish();
            }

            @Override
            public void onFailure(VolleyError error) {
                Log.e(TAG, "Failed to fetch assets required to determine notification state: " + error);
                listener.onFinish();
            }

        }).fetch();
    }

    private Notification createNotification(boolean silent, String userDisplayName, String roomUri, boolean isOneToOne, Bitmap roomAvatar, List<NotificationAssetsFetcher.Message> messages, boolean hasMentions) {

        RemoteInput remoteInput = new RemoteInput.Builder(REMOTE_INPUT_REPLY_KEY)
                .setLabel(res.getString(R.string.chat_input_placeholder))
                .build();

        NotificationCompat.Action replyAction = new NotificationCompat.Action.Builder(0, res.getString(R.string.reply), createReplyIntent(roomId, context))
                .addRemoteInput(remoteInput)
                // smart replies via android wear
                .setAllowGeneratedReplies(true)
                .build();

        NotificationCompat.MessagingStyle messagingStyle = new NotificationCompat.MessagingStyle(userDisplayName);
        NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();

        for (NotificationAssetsFetcher.Message message : messages) {
            String sender = (String) message.sender;
            messagingStyle.addMessage(message.text, message.timeStamp, userDisplayName.equals(sender) ? null : sender);
            inboxStyle.addLine(createInboxLine(sender, message.text));
        }

        if (!isOneToOne) {
            messagingStyle.setConversationTitle(roomUri);
            inboxStyle.setBigContentTitle(roomUri);
        }

        NotificationAssetsFetcher.Message firstMessage = messages.get(0);
        NotificationAssetsFetcher.Message lastMessage = messages.get(messages.size() - 1);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        builder.setContentIntent(createTapNotificationIntent(roomId, context))
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setOnlyAlertOnce(silent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSmallIcon(R.drawable.ic_stat_notify)
                .setLargeIcon(createLargeIcon(roomAvatar))
                .setContentTitle(isOneToOne ? firstMessage.sender : roomUri)
                .setContentText(isOneToOne ? firstMessage.text : createInboxLine(lastMessage.sender, lastMessage.text))
                .setColor(res.getColor(hasMentions ? R.color.mention : R.color.unread))
                .setShowWhen(true)
                .setWhen(lastMessage.timeStamp)
                .setStyle(BuildCompat.isAtLeastN() ? messagingStyle : inboxStyle);

        if (BuildCompat.isAtLeastN()) {
            builder.addAction(replyAction);
        }

        return builder.build();
    }

    private PendingIntent createTapNotificationIntent(String roomId, Context context) {
        Intent openChatIntent = new Intent(context, MainActivity.class);
        openChatIntent.putExtra(MainActivity.GO_TO_ROOM_ID_INTENT_KEY, roomId);
        // FLAG_ACTIVITY_CLEAR_TOP will clear the backstack so that a MainActivity is at the top
        // FLAG_ACTIVITY_SINGLE_TOP means that we will reuse a MainActivity if it is at the top of the backstack
        openChatIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        // FLAG_CANCEL_CURRENT ensures that we dont get an old cached pending intent
        return PendingIntent.getActivity(context, roomId.hashCode(), openChatIntent, PendingIntent.FLAG_CANCEL_CURRENT);
    }

    private PendingIntent createReplyIntent(String roomId, Context context) {
        if (BuildCompat.isAtLeastN()) {
            // supports direct reply
            Intent replyIntent = new Intent(context, NotificationReplyService.class);
            replyIntent.putExtra(ROOM_ID_INTENT_KEY, roomId);
            return PendingIntent.getService(context, roomId.hashCode(), replyIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        } else {
            return createTapNotificationIntent(roomId, context);
        }
    }

    private CharSequence createInboxLine(CharSequence sender, CharSequence text) {
        return TextUtils.concat(sender, ": ", text);
    }

    private Bitmap createLargeIcon(Bitmap avatar) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
            return new AvatarUtils(context).copyAsCircle(avatar);
        } else {
            return avatar;
        }
    }

    public interface Listener {
        void onFinish();
    }
}
